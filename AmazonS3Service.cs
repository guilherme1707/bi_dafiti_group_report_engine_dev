﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Threading.Tasks;


namespace excelDataSource
{
    class AmazonS3Service
    {
        //private const string bucketName = "dft-bi-report";
        //private string bucketName = "bi-dafiti-group-reports";
        private string bucketName;
        //private const string keyName = "*** provide a name for the uploaded object ***";
        //public static string S3URL;
        //private const string filePath = "*** provide the full path name of the file to upload ***";
        // Specify your bucket region (an example region is shown).
        private RegionEndpoint bucketRegion = RegionEndpoint.USEast1;
        private static IAmazonS3 s3Client;

        public AmazonS3Service(string bucketName, string accessKey, string secretKey)
        {
            this.bucketName = bucketName;
            Startup.log("UPLOADING FILE TO S3...");
            s3Client = new AmazonS3Client(accessKey, secretKey, bucketRegion);
        }

        public void UploadFileToBucket(string filePath, string fileKey) {
            UploadFileAsync(filePath, fileKey).Wait();
        }

        private async Task UploadFileAsync(string filePath, string fileKey)
        {
            try
            {
                var fileTransferUtility = new TransferUtility(s3Client);

                 //Specify object key name explicitly and upload the file
                await fileTransferUtility.UploadAsync(
                    String.Format(filePath) //file path
                    , String.Format(bucketName) //bucket name
                    , String.Format(fileKey) // key name (use filename as key)
                    );

                Startup.log("UPLOAD COMPLETED");
            }
            catch (AmazonS3Exception e)
            {
                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = ERROR");
                Startup.log("MESSAGE \t\t = ERROR ENCOUNTERED ON SERVER. COULD NOT UPLOAD FILE");
                Startup.log("DESCRIPTION \t = " + e.ToString());
                Startup.log("******************************************************************");
                Environment.Exit(1);
            }
            catch (Exception e)
            {
                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = UNKNOW");
                Startup.log("MESSAGE \t\t = UNKNOW ENCOUNTERED ON SERVER. COULD NOT UPLOAD FILE");
                Startup.log("DESCRIPTION \t = " + e.ToString());
                Startup.log("******************************************************************");
                Environment.Exit(1);
            }

        }

        public String GeneratePreSignedURL(string fileKey)
        {
            try
            {
                Startup.log("GENERATING LINK TO DOWNLOAD FOR KEY \t= " + fileKey);
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = String.Format(bucketName),
                    Key = String.Format(fileKey),
                    Expires = DateTime.Now.AddDays(7)
                };
                return s3Client.GetPreSignedURL(request);
            }
            catch (AmazonS3Exception e)
            {
                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = ERROR");
                Startup.log("MESSAGE \t\t = ERROR ENCOUNTERED ON SERVER. COULD NOT GENERATE S3 LINK");
                Startup.log("DESCRIPTION \t = " + e.ToString());
                Startup.log("******************************************************************");
                Environment.Exit(1);
                return null;
            }
            catch (Exception e)
            {
                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = UNKNOW");
                Startup.log("MESSAGE \t\t = UNKNOW ENCOUNTERED ON SERVER. COULD NOT GENERATE S3 LINK");
                Startup.log("DESCRIPTION \t = " + e.ToString());
                Startup.log("******************************************************************");
                Environment.Exit(1);
                return null;
            }
        }


    }
}