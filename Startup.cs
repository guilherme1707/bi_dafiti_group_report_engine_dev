﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Json;

namespace excelDataSource
{
    class Startup
    {
        private static JsonObject config;
        private static TextReader reader;
        private static string attachmentPath;

        public static string getValue(string paramenter)
        {
            try
            {
                string ret = config[paramenter];
                return ret;
            }
            catch (Exception e)
            {
                log("******************************************************************");
                log("TYPE \t\t = ERROR");
                log("MESSAGE \t\t = PARAMENTER " + paramenter + " NOT FOUND IN config.json");
                log("DESCRIPTION \t = " + e.ToString());
                log("******************************************************************");
                Environment.Exit(1);
                return null;
            }
        }

        // log with the current date timestamp
        public static void log(string text)
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " - " + text);
        }

        //kill excel instances
        public static void killExcelInstances()
        {
            Process[] processes = Process.GetProcessesByName("EXCEL");
            foreach (Process process in processes)
            {
                try
                {
                    process.Kill();
                }
                catch (Exception e)
                {
                    log("******************************************************************");
                    log("TYPE \t\t = WARNING");
                    log("MESSAGE \t\t = IT IS NOT POSSIBLE TO KILL AN INSTANCE");
                    log("DESCRIPTION \t = " + e.ToString());
                    log("******************************************************************");
                }
            }
        }

        /*compress file*/
        public static string CompressFile(string path)
        {
            string newFileName = path + ".gz";
            FileStream sourceFile = File.OpenRead(path);
            FileStream destinationFile = File.Create(newFileName);
            FileInfo infoSourceFole = new FileInfo(sourceFile.Name);
            FileInfo infoDestinationFile = new FileInfo(destinationFile.Name);

            byte[] buffer = new byte[sourceFile.Length];
            sourceFile.Read(buffer, 0, buffer.Length);

            using (GZipStream output = new GZipStream(destinationFile,
                CompressionMode.Compress))
            {
                log("COMPRESSING " + infoSourceFole.Name + " TO " + infoDestinationFile.Name + " ...");

                output.Write(buffer, 0, buffer.Length);
            }

            // Close the files.
            sourceFile.Close();
            destinationFile.Close();
            return newFileName;
        }

        //MAIN METHOD
        public static void Main(String[] args)
        {
            //killExcelInstances();
            Dictionary<string, string> arguments = new Dictionary<string, string>();
            /******************************************************************************
            * FIRST OF ALL VERIFY THE ARGUMENTS TO EXECUTE THE PROCESS
            * FILE_NAME, EMAIL_LIST and SUBJECT mandatory
            ******************************************************************************/
            foreach (string arg in args)
            {
                string[] param = arg.Split('=');
                arguments.Add(param[0].ToUpper(), param[1]);
            }
            try
            {
                string arg1;
                string arg2;
                string arg3;
                arg1 = arguments["EMAIL_LIST"];
                arg2 = arguments["FILE_NAME"];
                arg3 = arguments["SUBJECT"];
                log("******************************************************************");
                log("ARGUMENTS");
                log("******************************************************************");
                log("FILE_NAME \t = " + arg2);
                log("EMAIL_LIST \t = " + arg1);
                log("SUBJECT \t\t = " + arg3);
            }
            catch (Exception e)
            {
                log("******************************************************************");
                log("TYPE \t\t = ERROR");
                log("MESSAGE \t\t = FILE_NAME, EMAIL_LIST OR SUBJECT IS NOT DEFINED");
                log("DESCRIPTION \t = " + e.ToString());
                log("******************************************************************");
                Environment.Exit(1);
            }
            /*
             * TRY READ THE CONFIG FILE
            */
            try
            {
                if (!File.Exists("config.json"))
                {
                    log("******************************************************************");
                    log("TYPE \t\t = FILE NOT FOUND");
                    log("MESSAGE \t\t = CONFIG FILE DOES NOT EXISTS");
                    log("DESCRIPTION \t = ");
                    log("******************************************************************");
                    Environment.Exit(1);
                }
                reader = File.OpenText("config.json");
                config = (JsonObject)JsonObject.Load(reader);
                reader.Close();
            }
            catch (Exception e)
            {
                reader.Close();
                log("******************************************************************");
                log("TYPE \t\t = ERROR");
                log("MESSAGE \t\t = FILE IN WRONG FORMAT");
                log("DESCRIPTION \t = NOT ABLE TO PARSE CONFIG FILE \n" + e.ToString());
                log("******************************************************************");
                Environment.Exit(1);
            }

            //verify if the file not exists
            string fileFolder = getValue("report_folder");
            string copy = DateTime.Now.ToString("yyyyMMdd") + " " + arguments["FILE_NAME"];
            string filePath = fileFolder + arguments["FILE_NAME"];
            if (!File.Exists(filePath))
            {
                if (arguments.ContainsKey("INCLUDE"))
                {
                    /*
                     * UPLOAD THE FILE TO S3 BUCKET
                     */
                    AmazonS3Service S3Sevice = new AmazonS3Service(
                          getValue("s3_bucket_name")
                        , getValue("s3_bucket_access_key")
                        , getValue("s3_bucket_secret_key")
                        );
                    // list to store all links to send in the e-mail
                    List<string> links = new List<string>();
                    foreach (string fileKey in arguments["INCLUDE"].Trim().Split(','))
                    {
                        links.Add(S3Sevice.GeneratePreSignedURL(fileKey));
                    }
                    /*
                     * SEND E-MAIL WITH THE UPDATED FILE
                     */
                    log("******************************************************************");
                    log("SENDING E-MAIL...");
                    log("******************************************************************");
                    // configure and send the em-ail
                    Mail email = new Mail(getValue("email_sender")
                        , getValue("email_password")
                        , arguments["EMAIL_LIST"]
                        , arguments["SUBJECT"]);
                    string mailBody = "";
                    if (links.Count > 1)
                    {
                        mailBody += "<p>Your CSV export(s) can be downloaded here:</p>";
                        for (int i = 1; i < links.Count; i++)
                        {
                            mailBody += "<a href='" + links[i] + "'>" + links[i] + "</a><br/>";
                        }
                    }

                    email.setBody(mailBody);
                    email.send();

                } else
                {
                    log("******************************************************************");
                    log("TYPE \t\t = ERROR");
                    log("MESSAGE \t\t = FILE NOT EXISTS");
                    log("DESCRIPTION \t = THE FILE WAS NOT FOUND. THE PROCESS IS ENDING");
                    log("******************************************************************");
                    Environment.Exit(1);
                }

            }
            /*
             * REFRESH ALL CONNECTIONS IN EXCEL FILE
            */
            log("******************************************************************");
            log("STARTING PROCESS...");
            log("******************************************************************");
            log("OPENING EXCEL FILE AND REFRESHING CONNECTIONS...");
            //open the excel file
            //refresh connection and pivot tables if exists
            Excel excelFile = new Excel(filePath);
            excelFile.refreshConnections();
            if (arguments.ContainsKey("IGNORE_PIVOT_TABLES"))
            {
                if (!arguments["IGNORE_PIVOT_TABLES"].Equals("1"))
                {
                    excelFile.refreshPivotTables();
                }
                else
                {
                    log("IGNORING PIVOT TABLES REFRESH..");
                }
            }
            else
            {
                excelFile.refreshPivotTables();
            }
            excelFile.saveAndClose();

            //create a copy of the file prepending the current date in the name to send in the email and upload in the bucket
            File.Copy(filePath, fileFolder + copy, true);
            attachmentPath = fileFolder + copy;
            log("CREATED TEMPORARY COPY FILE");
            log("TEMPORARY FILE NAME = " + copy);

            //compress file if needed
            if (arguments.ContainsKey("COMPRESSED"))
            {
                if (arguments["COMPRESSED"].Equals("1"))
                {
                    attachmentPath = CompressFile(fileFolder + copy);
                }
            }

            /*
             * UPLOAD THE FILE TO S3 BUCKET
            */
            AmazonS3Service amazonSevice = new AmazonS3Service(
                  getValue("s3_bucket_name")
                , getValue("s3_bucket_access_key")
                , getValue("s3_bucket_secret_key")
                );
            // list to store all links to send in the e-mail
            List<string> linkList = new List<string>();
            string bucketFileKey = DateTime.Now.ToString("yyyyMMdd") + "/" + copy;
            amazonSevice.UploadFileToBucket(attachmentPath, bucketFileKey);
            linkList.Add(amazonSevice.GeneratePreSignedURL(bucketFileKey));

            if (arguments.ContainsKey("INCLUDE"))
            {
                foreach (string fileKey in arguments["INCLUDE"].Trim().Split(','))
                {
                    linkList.Add(amazonSevice.GeneratePreSignedURL(fileKey));
                }
            }

            /*
             * SEND E-MAIL WITH THE UPDATED FILE
            */
            log("******************************************************************");
            log("SENDING E-MAIL...");
            log("******************************************************************");
            // configure and send the em-ail
            Mail mail = new Mail(getValue("email_sender")
                , getValue("email_password")
                , arguments["EMAIL_LIST"]
                , arguments["SUBJECT"]);
            mail.addAttachment(attachmentPath);
            string htmlBody = "<p>Your file can be downloaded here</p><a href='" + linkList[0] + "'>" + linkList[0] + "</a><br/>";
            if (linkList.Count > 1)
            {
                htmlBody += "<p>You have more files included:</p>";
                for (int i = 1; i < linkList.Count; i++)
                {
                    htmlBody += "<a href='" + linkList[i] + "'>" + linkList[i] + "</a><br/>";
                }
            }

            mail.setBody(htmlBody);
            mail.send();

            log("DELETING TEMPORARY FILE..");
            //delete temporary file
            if (File.Exists(fileFolder + copy))
            {
                File.Delete(fileFolder + copy);
            }

            if (File.Exists(attachmentPath))
            {
                File.Delete(attachmentPath);
            }
            if (!mail.isSent())
            {
                Environment.Exit(1);
            }
        }
    }
}