﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Office.Interop.Excel;

namespace excelDataSource
{
    class Excel
    {
        private Application app;
        private Workbook workbook;

        public Excel() { }

        public Excel(string filePath)
        {
            this.setFile(filePath);
        }

        public void setFile(string filePath)
        {
            this.app = new Application();
            this.app.Visible = false;
            this.app.DisplayAlerts = false;
            this.workbook = app.Workbooks.Open(filePath);
        }

        public void refreshConnections()
        {
            //try to find any connection in excel file
            try
            {
                var tryGetConnections = workbook.Connections;
            }
            catch (Exception e)
            {

                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = ERROR");
                Startup.log("MESSAGE \t\t = EXTERNAL CONNECTION NOT FOUND");
                Startup.log("DESCRIPTION \t = CLOSING EXCEL FILE\n" + e.ToString());
                Startup.log("******************************************************************");
                this.saveAndClose();
                Environment.Exit(1);
            }
            //loop through each ODBC connection and refresh
            int connections = workbook.Connections.Count;
            Startup.log("CONNECTIONS: " + connections);
            int connectionsCounter = 1;
            foreach (WorkbookConnection conn in workbook.Connections)
            {
                Startup.log("REFRESHING CONNECTION " + connectionsCounter + ": " + conn.Name);
                conn.ODBCConnection.BackgroundQuery = false;
                try
                {
                    conn.Refresh();
                }
                catch (Exception refresh)
                {

                    Startup.log("******************************************************************");
                    Startup.log("TYPE \t\t = ERROR");
                    Startup.log("MESSAGE \t\t = IT IS NOT POSSIBLE TO UPDATE THE CONNECTION");
                    Startup.log("DESCRIPTION \t = CLOSING EXCEL FILE\n" + refresh.ToString());
                    Startup.log("******************************************************************");
                    this.saveAndClose();
                    Environment.Exit(1);
                }
                connectionsCounter++;
            }
        }


        public void refreshPivotTables()
        {
            Startup.log("REFRESHING PIVOT TABLES IF EXISTS...");

            List<string> connectionsName = new List<string>();
            foreach (Worksheet ws in workbook.Worksheets)
            {
                PivotTables pt = (PivotTables)ws.PivotTables();
                int pivotCount = pt.Count;
                Startup.log(pivotCount + " PIVOT TABLE(S) IN WORKSHEET " + ws.Name);
                if (pivotCount > 0)
                {
                    for (int i = 1; i <= pivotCount; i++)
                    {
                        try
                        {
                            //pt.Item(i).RefreshTable();
                            WorkbookConnection conn;
                            try
                            {
                                conn = pt.Item(i).PivotCache().WorkbookConnection;
                            }
                            catch (Exception)
                            {
                                conn = null;
                            }

                            if (conn == null || !connectionsName.Contains(conn.Name)) {
                                Startup.log("REFRESHING PIVOT TABLE " + i);
                                pt.Item(i).PivotCache().Refresh();
                                if(conn != null)
                                {
                                    connectionsName.Add(conn.Name);
                                }
                            } else
                            {
                                Startup.log("NOT NEEDED TO REFRESH PIVOT TABLE " + i + " IN SHEET " + ws.Name);
                            }
                        }
                        catch (Exception refresh)
                        {
                            Startup.log("******************************************************************");
                            Startup.log("TYPE \t\t = ERROR");
                            Startup.log("MESSAGE \t\t = IT IS NOT POSSIBLE TO UPDATE THE PIVOT TABLE");
                            Startup.log("DESCRIPTION \t = CLOSING EXCEL FILE\n" + refresh.ToString());
                            Startup.log("******************************************************************");
                            this.saveAndClose();
                            Environment.Exit(1);
                        }
                    }
                }
            }
        }

        public void saveAndClose()
        {
            try
            {
                Startup.log("SAVING AND CLOSING FILE...");
                //save and close
                workbook.Save();
                workbook.Close();
                app.Workbooks.Close();
                app.Quit();
                Startup.log("FILE SAVED AND CLOSED");
            }
            catch (Exception ex)
            {
                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = CRITICAL ERROR");
                Startup.log("MESSAGE \t\t = COULD NOT SAVE AND CLOSE FILE");
                Startup.log("DESCRIPTION \t = " + ex.ToString());
                Startup.log("******************************************************************");
                Environment.Exit(1);
            }
        }
    }
}
