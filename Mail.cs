﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Mail;

namespace excelDataSource
{
    class Mail
    {
        private MailMessage mail;
        private SmtpClient client;
        private string from;
        private string to;
        private Boolean sent;

        /**
         * constructor
         */
        public Mail(string emailFrom, string pass, string emailTo, string subject) {
            mail = new MailMessage();
            client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(emailFrom, pass);
            mail.IsBodyHtml = true;
            mail.BodyEncoding = Encoding.UTF8;
            this.setFrom(emailFrom);
            this.setTo(emailTo);
            this.setSubject(subject);
        }

        public void setFrom(string from)
        {
            this.from = from;
            this.mail.From = new MailAddress(from);
        }

        public string getFrom()
        {
            return this.from;
        }

        public void setTo(string to)
        {
            this.to = to.Trim();
            this.mail.To.Clear();
            foreach(string str in to.Trim().Split(','))
            {
                //this.mail.To.Add(str);
                this.mail.Bcc.Add(str);
            }
        }

        public string getTo()
        {
            return this.to;
        }

        public void setPort(int port)
        {
            this.client.Port = port;
        }

        public int getPort()
        {
            return this.client.Port;
        }

        public void setHost(string host)
        {
            client.Host = host;
        }

        public string getHost()
        {
            return client.Host;
        }

        public void setSubject(string subject)
        {
            mail.Subject = subject + " " + DateTime.Now.ToString("dd-MM-yyyy");
        }

        public string getSubject()
        {
            return mail.Subject;
        }

        public void setBody(string body)
        {
            mail.Body = body;
        }

        public string getBody()
        {
            return this.mail.Body;
        }

        public void isBodyHTML(bool html)
        {
            mail.IsBodyHtml = html;
        }

        public Boolean isSent()
        {
            return sent;
        }

        public void addAttachment(string filePath)
        {
            mail.Attachments.Add(new Attachment(filePath));
            mail.Attachments.Dispose();
        }

        /*
         * try to send the e-mail
         */
        public void send()
        {
            Startup.log("SENDIG TO: \n" + this.to.Trim().Replace(',', '\n'));
            try
            {
                try
                {
                    client.Send(mail);
                    this.sent = true;
                }
                catch (Exception e)
                {
                    Startup.log("******************************************************************");
                    Startup.log("TYPE \t\t = WARNING");
                    Startup.log("MESSAGE \t\t = IT WAS NOT POSSIBLE TO SEND THE E-MAIL");
                    Startup.log("DESCRIPTION \t = SOMETHING WENT WRONG AND THE E-MAIL COULD NOT BE SENT\n" + e.ToString());
                    Startup.log("******************************************************************");
                    Startup.log("RETRYING SEND E-MAIL");
                    Thread.Sleep(5000);

                    client.Send(mail);
                    this.sent = true;
                }

                Startup.log("E-MAIL SUCCESSFULLY SENT");
            }
            catch (Exception err)
            {
                Startup.log("******************************************************************");
                Startup.log("TYPE \t\t = ERROR");
                Startup.log("MESSAGE \t\t = IT WAS NOT POSSIBLE TO SEND THE MAIL");
                Startup.log("DESCRIPTION \t = SOMETHING WENT WRONG AND THE E-MAIL COULD NOT BE SENT\n" + err.ToString());
                Startup.log("******************************************************************");
                //mail.Attachments.Dispose();
                this.sent = false;
            }
            
            //mail.Attachments.Dispose();
        }

    }
}
